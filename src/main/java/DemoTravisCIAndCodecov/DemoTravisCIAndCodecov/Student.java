package DemoTravisCIAndCodecov.DemoTravisCIAndCodecov;

public class Student {
	private double score;

	/**
	 * @param score
	 */
	public Student(double score) {
		super();
		this.score = score;
	}

	public double compute() {
		return this.score * 4 / 10;
	}
}
