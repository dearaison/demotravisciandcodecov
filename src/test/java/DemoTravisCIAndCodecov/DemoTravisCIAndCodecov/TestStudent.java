package DemoTravisCIAndCodecov.DemoTravisCIAndCodecov;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestStudent {
	@Test
	public void testCompute() {
		Student student = new Student(9.0);
		assertEquals(3.6, student.compute(), 0.01);
	}
}
